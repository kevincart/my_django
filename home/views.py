from django.http import HttpResponse
import datetime
now = datetime.datetime.now()


def index(request):
    timeNow = now.strftime(" The time is %Y-%m-%d %H:%M:%S")
    indexResponse = "Hello, world. You're at the polls index."
    return HttpResponse(indexResponse + timeNow)